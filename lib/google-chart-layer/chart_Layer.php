<?php
  include("lib/api/functions.php");
  include("lib/db/libsql.php");
  
  $sql = new SQLManager();
  
  function delta_time ($data_iniziale,$data_finale,$unita) {
 
    $data1 = strtotime($data_iniziale);
    $data2 = strtotime($data_finale);
    
       switch($unita) {
           case "m": $unita = 1/60; break; 	//MINUTI
           case "h": $unita = 1; break;		//ORE
           case "g": $unita = 24; break;		//GIORNI
           case "a": $unita = 8760; break;         //ANNI
       }
    
    $differenza = (($data2-$data1)/3600)/$unita;
    return $differenza;
   }
   
  function getBuzzIndexes($Monitor){
    $i=0;
    foreach ($Monitor->categories as $Category){
      if      ($Category->category=="Basic Positive")
        $BasicPositive=$i;
      elseif  ($Category->category=="Basic Negative")
        $BasicNegative=$i;
      elseif  ($Category->category=="Basic Neutral")
        $BasicNeutral=$i;
      $i++;
    }
    return (array("Positive"=>$BasicPositive,"Negative"=>$BasicNegative,"Neutral"=>$BasicNeutral));
  }
  
  function record_sort($records, $field, $reverse=false)
  {
    $hash = array();
    
    foreach($records as $record)
    {
        $hash[$record[$field]] = $record;
    }
    
    ($reverse)? krsort($hash) : ksort($hash);
    
    $records = array();
    
    foreach($hash as $record)
    {
        $records []= $record;
    }
    
    return $records;
  }

  
  function getSentimentBuzzChart($id,$divId, $dateStart, $dateEnd, $showHL=TRUE){
    
    $config = new Config;
    
    $Monitors=getMonitorResultsBuzz($id,$dateStart, $dateEnd);
    
    $Positive_vol=0;
    $Neutral_vol=0;
    $Negative_vol=0;
    
    $Positive_props="";
    $Neutral_props="";
    $Negative_props="";
    $NPS_props="";
    
    foreach ($Monitors as $Monitor){
      
      $Pos_index=getBuzzIndexes($Monitor);
      
      $Positive_vol+=$Monitor->categories[$Pos_index["Positive"]]->volume;
      $Neutral_vol+=$Monitor->categories[$Pos_index["Neutral"]]->volume;
      $Negative_vol+=$Monitor->categories[$Pos_index["Negative"]]->volume;
      
      $Positive_props.=($Monitor->categories[2]->proportion*100).",";
      $Neutral_props.=($Monitor->categories[0]->proportion*100).",";
      $Negative_props.=($Monitor->categories[1]->proportion*100).",";
      
      $NPS_props.=($Monitor->categories[2]->proportion*100-$Monitor->categories[1]->proportion*100).",";
    }
    $Positive_props=substr($Positive_props, 0, strlen($Positive_props)-1);
    $Neutral_props=substr($Neutral_props, 0, strlen($Neutral_props)-1);
    $Negative_props=substr($Negative_props, 0, strlen($Negative_props)-1);
    $NPS_props=substr($NPS_props, 0, strlen($NPS_props)-1);
    
    $Positive_prop=round(($Positive_vol/($Positive_vol+$Neutral_vol+$Negative_vol))*100);
    $Neutral_prop=round(($Neutral_vol/($Positive_vol+$Neutral_vol+$Negative_vol))*100);
    $Negative_prop=100-$Positive_prop-$Neutral_prop;
    
    
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Sentiment',                                 'Percentage',         { role: 'style' },                                                              { role: 'annotation' }],
          ['  ".(($showHL) ? 'POSITIVE' : '')."',      ".$Positive_prop.",   'color: ".$config->PositiveBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Positive_prop."%'],
          ['  ".(($showHL) ? 'NEUTRAL' : '')."',       ".$Neutral_prop.",    'color: ".$config->NeutralBuzzColor." ; opacity: ".$config->BuzzOpacity."',     '".$Neutral_prop."%'],
          ['  ".(($showHL) ? 'NEGATIVE' : '')."',      ".$Negative_prop.",   'color: ".$config->NegativeBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Negative_prop."%']
        ]);
        
        var options = {
          title: 'Related Posts=".($Positive_vol+$Neutral_vol+$Negative_vol)." | NPS=".($Positive_prop-$Negative_prop)."',
          bar: {groupWidth: \"90%\"},
          fontName: 'Open Sans',
          vAxis: {
                    maxValue:100,
                    textPosition: 'none'
                  },
          legend: { position: \"none\" },
          chartArea:{left:25,top:20,width:'90%',height:'80%'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('".$divId."'));
        chart.draw(data, options);
      }
    </script>";
    
    $back_values=array(
                       "chart"=>$chart,
                       "TotPos"=>$Positive_vol,
                       "TotNeu"=>$Neutral_vol,
                       "TotNeg"=>$Negative_vol,
                       "TotNPS"=>($Positive_prop-$Negative_prop),
                       "PosTrend"=>$Positive_props,
                       "NeuTrend"=>$Neutral_props,
                       "NegTrend"=>$Negative_props,
                       "NPSTrend"=>$NPS_props,
                       "Pos"=>$Positive_prop,
                       "Neu"=>$Neutral_prop,
                       "Neg"=>$Negative_prop
                      );
    
    return $back_values;

  }
  
  function getSoVChart($divId, $Brands){
    
    $config = new Config;
    $Keys=array_keys($Brands);
    
    
    $data="['Brand', 'Value',{ role: 'annotation' }],";
    foreach ($Keys as $Key){
      $data.="['".$Key."',".$Brands[$Key].",'".$Key."'],";
    }
    
    
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ".$data." // CSS-style declaration
      ]);
        
        var options = {
          title: 'Share of Voice',
          legend: { position: \"labeled\" },
          chartArea:{left:0,top:10,width:'90%',height:'90%'},
          pieHole:0.4,
          pieSliceText: \"none\",
         
          slices: {
            0: { color: '".$config->BrandsColors["Samsung"]."',  },
            1: { color: '".$config->BrandsColors["Nokia"]."',  },
            2: { color: '".$config->BrandsColors["LG"]."',  },
            3: { color: '".$config->BrandsColors["Hwawei"]."',  },
            4: { color: '".$config->BrandsColors["Apple"]."',  }
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('".$divId."'));
        chart.draw(data, options);
      }
    </script>";
    $back_values=array(
                       "chart"=>$chart
                    );
  
    return $back_values;
  }
  
  
  function getSourcesChart($id,$divId, $dateStart, $dateEnd){
    
    $config = new Config;
    $Monitors=getSources($id,$dateStart, $dateEnd);
    $max_sites=10;
    $topSiteArray=array();
    
    foreach ($Monitors as $Monitor)
      foreach ($Monitor->topSites as $site){
        $topSiteArray[$site->name]+=$site->count;
      }
    
    arsort($topSiteArray);
    $Keys=array_keys($topSiteArray);
    $data="['Site', 'Value', { role: 'style' }],";
    $n_site=0;
    foreach ($Keys as $Key){
      if ($n_site<$max_sites){
        $data.="['".$Key."',".$topSiteArray[$Key].",'rgb(79,129,189)'],";
        $n_site++;
      }
    }
    
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ".$data." // CSS-style declaration
      ]);
        
        var options = {
          title: 'Top Sites',
          legend: { position: \"none\" },
          chartArea:{left:100,top:10,width:'90%',height:'85%'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('".$divId."'));
        chart.draw(data, options);
      }
    </script>";
    
    foreach ($Monitors[0]->sources as $source){
      $topSourcesArray[$source->name]=$source->count;
    }
    

    
    $back_values=array(
                       "chart"=>$chart,
                       "sources"=>$topSourcesArray
                    );
  
    return $back_values;
  }
  
  function getSoVBChart($divId, $Brands){
    
    $config = new Config;
    $Keys=array_keys($Brands);
    
    $i=0;
    $colors="";
    $data="['Brand', 'NPS', 'SoV', 'Brand', 'Total Posts'],";
    foreach ($Keys as $Key){
      $data.="['". $Key ."',".$Brands[$Key]["NPS"].",".$Brands[$Key]["Share"].",". $i .",". $Brands[$Key]["TotPost"] ."],"; $i++;
      $colors.="'".$config->BrandsColors[$Key]."',";
    }
    
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ".$data." // CSS-style declaration
      ]);
        
        var options = {
          colorAxis: {colors: [".$colors."]},
          title: 'Share of Voice',
          hAxis: {title: 'NPS'},
          vAxis: {title: 'SoV'},
          legend: { position: \"none\" },
          hAxis: {  maxValue:10,
                    minValue:-15,
                    title:'NPS'
                  },
          vAxis: {  maxValue:0.65,
                    minValue: 0,
                    format:'#%',
                    title:'Share of Voice',
                    
                  },
          chartArea:{left:40,top:10,bottom:10,width:'85%',height:'80%'},
      }
      var chart = new google.visualization.BubbleChart(document.getElementById('".$divId."'));
      chart.draw(data, options);
    }
    </script>";
    $back_values=array(
                       "chart"=>$chart
                      );
  
    return $back_values;
  }
  
  function getSentimentPerSourceChart($id, $sources, $dateStart, $dateEnd){
    $config = new Config;
    $PositiveScore=0;
    $NegativeScore=0;
    $NeutralScore=0;
    $index=0;
    foreach ($sources as $sourcename => $sourcecount){
      $posts=getMonitorPosts($id,$dateStart,$dateEnd,array("type:".$sourcename));
      foreach ($posts as $post){
        foreach($post->categoryScores as $category)
          if ($category->categoryName=="Basic Positive") 
            $PositiveScore=$category->score;
          else if ($category->categoryName=="Basic Neutral")
            $NeutralScore=$category->score;
          else if ($category->categoryName=="Basic Negative")
            $NegativeScore=$category->score;
            
          if (($PositiveScore>$NeutralScore) && ($PositiveScore>$NegativeScore))
            $SourceSentimentSplit[$sourcename]["Positive"]+=1;
          if (($NeutralScore>$PositiveScore) && ($NeutralScore>$NegativeScore))
            $SourceSentimentSplit[$sourcename]["Neutral"]+=1;
          if (($NegativeScore>$NeutralScore) && ($NegativeScore>$PositiveScore))
            $SourceSentimentSplit[$sourcename]["Negative"]+=1;
          $PositiveScore=0;
          $NegativeScore=0;
          $NeutralScore=0;
        }
        $Positive_prop=round(($SourceSentimentSplit[$sourcename]["Positive"]/($SourceSentimentSplit[$sourcename]["Positive"]+$SourceSentimentSplit[$sourcename]["Neutral"]+$SourceSentimentSplit[$sourcename]["Negative"]))*100);
        $Neutral_prop=round(($SourceSentimentSplit[$sourcename]["Neutral"]/($SourceSentimentSplit[$sourcename]["Positive"]+$SourceSentimentSplit[$sourcename]["Neutral"]+$SourceSentimentSplit[$sourcename]["Negative"]))*100);
        $Negative_prop=100-$Positive_prop-$Neutral_prop;
        
        $showHL=FALSE;
        $chart[]=
        "<script type=\"text/javascript\">
        google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
          var data = google.visualization.arrayToDataTable([
            ['Sentiment',                                 'Percentage',         { role: 'style' },                                                              { role: 'annotation' }],
            ['  ".(($showHL) ? 'POSITIVE' : '')."',      ".$Positive_prop.",   'color: ".$config->PositiveBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Positive_prop."%'],
            ['  ".(($showHL) ? 'NEUTRAL' : '')."',       ".$Neutral_prop.",    'color: ".$config->NeutralBuzzColor." ; opacity: ".$config->BuzzOpacity."',     '".$Neutral_prop."%'],
            ['  ".(($showHL) ? 'NEGATIVE' : '')."',      ".$Negative_prop.",   'color: ".$config->NegativeBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Negative_prop."%']
          ]);
          
          var options = {
            title: 'Related Posts=".($SourceSentimentSplit[$sourcename]["Positive"]+$SourceSentimentSplit[$sourcename]["Neutral"]+$SourceSentimentSplit[$sourcename]["Negative"])." | NPS=".($Positive_prop-$Negative_prop)."',
            bar: {groupWidth: \"90%\"},
            fontName: 'Open Sans',
            vAxis: {
                      maxValue:100,
                      textPosition: 'none'
                    },
            legend: { position: \"none\" },
            chartArea:{left:25,top:20,width:'90%',height:'80%'}
          };
  
          var chart = new google.visualization.ColumnChart(document.getElementById('".$sourcename."Sentiment'));
          chart.draw(data, options);
        }
      </script>";
      
      $divSources[]="<div class=\"quarter_size\"><div id=\"".$sourcename."Sentiment\" ></div></div>";
      
      $index++;
      
        
      }
    $back_values=array(
                       "chart"=>$chart,
                       "divs"=>$divSources
                      );
  
    return $back_values;
  }
  
  function getTwitterMetricsTablesChart($profileIds, $divId,$startDate, $endDate,$interval){
    $config = new Config;
    
    $values=getTwitterMetricsTables($profileIds, $startDate, $endDate,$interval);
    $jsonTwTab=$values["TwitterTable"];
    $jsonTwRT=$values["ResponseTime"];
    
    $data="['Brand', 'Response Time', 'Response Rate', 'Brand', 'Total Tweets'],";
    $i=0;
    $ResponseTimes=0;
    foreach($jsonTwRT["data"] as $jsonTwRTBrand){
      if (($jsonTwRTBrand["0h - 2h"]*1+$jsonTwRTBrand["2h - 8h"]*5+$jsonTwRTBrand["8h - 24h"]*16+$jsonTwRTBrand["24h+"]*25)==0)
        $ResponseTime=0;
      else
        $ResponseTime=($jsonTwRTBrand["0h - 2h"]*1+$jsonTwRTBrand["2h - 8h"]*5+$jsonTwRTBrand["8h - 24h"]*16+$jsonTwRTBrand["24h+"]*25)/($jsonTwRTBrand["0h - 2h"]+$jsonTwRTBrand["2h - 8h"]+$jsonTwRTBrand["8h - 24h"]+$jsonTwRTBrand["24h+"]);
      $data.="['".$jsonTwTab["data"][$i]["name"]."', ".$ResponseTime.", ".($jsonTwTab["data"][$i]["responseRate"]/100).", '".$jsonTwTab["data"][$i]["name"]."', ".$jsonTwTab["data"][$i]["nbTweets"]."],";
      $i++;
    }
    
    
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ".$data." // CSS-style declaration
      ]);
        
        var options = {
          
          title: 'Share of Voice',
          hAxis: {title: 'NPS'},
          vAxis: {title: 'SoV'},
          legend: { position: \"none\" },
          hAxis: {  maxValue:30,
                    minValue:-5,
                    title:'Response Time'
                  },
          vAxis: {  maxValue:0.99,
                    minValue: -0.1,
                    format:'#%',
                    title:'Response Rate',
                    
                  },
          chartArea:{left:70,top:10,bottom:20,width:'95%',height:'85%'},
      }
      var chart = new google.visualization.BubbleChart(document.getElementById('".$divId."'));
      chart.draw(data, options);
    }
    </script>";
    $back_values=array(
                       "chart"=>$chart
                      );
  
    return $back_values;
  
  }
  
  function MonitorResultMultiOpinionChart($profileIds, $startDate, $endDate, $mode, $divId, $order=["positiv", "neutr", "negativ"] ){

    $palette=new colorHarmony;
    $palette_mono["positiv"]=$palette->Monochromatic('#00a650');
    $palette_mono["neutr"]=$palette->Monochromatic('#898989');
    $palette_mono["negativ"]=$palette->Monochromatic('#ee1d24');

    //Organizzo il vettore corretto per l'organizzazione del grafico adeguato
    $monitors=getMonitorResultsOpinion($profileIds, $startDate, $endDate);
    $arrayChart=array();
    $TotalBuzzAmount=0;
    foreach ($monitors as $monitor) {
        foreach ($monitor->categories as $category) {
            if (($category->groupId != "") and (!$category->hidden)){
                $arrayChart[$category->group][$category->category]+=$category->volume;
                $TotalBuzzAmount+=$category->volume;

            } elseif (!$category->hidden){
                $arrayChart[$category->category]+=$category->volume;
                $TotalBuzzAmount+=$category->volume;
            }
        }
    }


    $headMex="";
    $bodyMex="";
    $headCommas="";
    $bodyCommas=",";
    $topMexAnnotation="";
    
    $seriesMex="";
    $seriesCount=0;
    $seriesIndex=0;
    $seriesVisibleLegend="false";

    $totalCategoryBuzz=0;
    //Un ordine deve essere sempre definito
    foreach ($order as $value) {  
        foreach ($arrayChart as $arrayChartKey => $arrayChartValue) {
            if ((stripos($arrayChartKey,$value) !== false)){
                if (is_array($arrayChartValue)){
                    foreach ($arrayChartValue as $arrayChartValueKey => $arrayChartValueValue) {
                        $headMex.=",".($arrayChartValueValue/$TotalBuzzAmount).",''";
                        $bodyMex="'".$arrayChartValueKey." [".$arrayChartValueValue."]'".$headCommas.$bodyCommas.($arrayChartValueValue/$TotalBuzzAmount).",'".round(($arrayChartValueValue/$TotalBuzzAmount)*100)."'";
                        $MexT[]=$bodyMex;
                        $bodyCommas.=",'',";
                        $topMexAnnotation.="'".$arrayChartValueKey."',{ role: 'annotation' },";

                        
                        $seriesMex.=$seriesIndex.": { visibleInLegend: ".$seriesVisibleLegend.", color: '".$palette_mono[$value][$seriesCount]."' },\n";
                        $seriesCount++; 
                        $seriesIndex++; 
                        $totalCategoryBuzz+=$arrayChartValueValue;  
                    }

                    $headMex=substr($headMex, 0, strlen($headMex)-2);
                    $headMex.="'".round(($totalCategoryBuzz/$TotalBuzzAmount)*100)."'";

                    $headMex="'".$arrayChartKey." [".$totalCategoryBuzz."]'".$headCommas.$headMex;
                    $Mex[]=$headMex;
                    $Mex=array_merge($Mex,$MexT);
                }
                else {
                    $topMexAnnotation.="'".$arrayChartKey."',{ role: 'annotation' },";
                    $headMex="'".$arrayChartKey." [".$arrayChartValue."]'".",".$headCommas.($arrayChartValue/$TotalBuzzAmount).",'".round(($arrayChartValue/$TotalBuzzAmount)*100)."'";
                    $Mex[]=$headMex;

                    $seriesMex.=$seriesIndex.": { visibleInLegend: ".$seriesVisibleLegend.", color: '".$palette_mono[$value][$seriesCount]."' },\n";
                    $seriesIndex++;
                }
                
                $seriesCount=0;
                unset($MexT);
                $bodyCommas=",";
                for ($i=0; $i < count($arrayChartValue); $i++) { 
                    $headCommas.=",'',";
                }
        }
      $totalCategoryBuzz=0;
      $headMex="";
      $bodyMex="";
      }
    }

    $maxelements=0;
    for ($i=0; $i < count($Mex); $i++) { 
      $elements=count(explode(",", $Mex[$i]));
      if ($elements>$maxelements)
        $maxelements=$elements;
      
    }
    for ($i=0; $i < count($Mex); $i++) { 
      $elements=count(explode(",", $Mex[$i]));
      
      for ($k=0; $k < ($maxelements-$elements); $k++) { 
        $Mex[$i].=",";
      }
      $Mex[$i].=",";
    }

    $topMexAnnotation=substr($topMexAnnotation, 0, strlen($topMexAnnotation)-1);
    $topMexAnnotation="['Genre',".$topMexAnnotation." ],";
    
    $seriesMex=substr($seriesMex, 0, strlen($seriesMex)-2);
    $seriesMex="series: {\n".$seriesMex."},\n";


    $data="";
    for ($i=0; $i < count($Mex); $i++) { 
      $data.="[".$Mex[$i]."],\n";
    }
    /*echo "<pre>";
    print_r($Mex);
    echo "<pre>";
*/
    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ".$topMexAnnotation.$data."
      ]);

      var view = new google.visualization.DataView(data);
      
      var options = {
        height: 400,
        annotationsWidth: 80,
        bar: {groupWidth: \"90%\"},
        chartArea:{left:200,top:20,width:'90%',height:'80%'},
        legend: { position: 'right', maxLines: 3 },
        ".$seriesMex."
        
        isStacked: true,
        hAxis: {    maxValue:1,
                    minValue: 0,
                    format:'#%',
                    
                    
                  },
      };
      var chart = new google.visualization.BarChart(document.getElementById('".$divId."'));
      chart.draw(data, options);
    }
    </script>";
    
    $back_values=array(
                       "chart"=>$chart
                      );
  
    return $back_values;
  }

  function MonitorResultSentimentOpinionChart($profileIds, $startDate, $endDate, $divId, $showHL=TRUE ){

    $config = new Config;
    $returnVals=array();

    //Organizzo il vettore corretto per l'organizzazione del grafico adeguato
    $monitors=getMonitorResultsOpinion($profileIds, $startDate, $endDate);

    $arrayChart=array();
    $arrayChartTrends=array();
    $arrayChartHidden=array();

    $TotalBuzzAmount=0;
    $TotalBuzzAmountHidden=0;

    foreach ($monitors as $monitor) {
        foreach ($monitor->categories as $category) {
            if (!$category->hidden){
                $arrayChart[$category->category]+=$category->volume;
                $arrayChartTrends[$category->category].=$category->volume.",";
                $TotalBuzzAmount+=$category->volume;
            } else {
                $arrayChartHidden[$category->category]+=$category->volume;
                $TotalBuzzAmountHidden+=$category->volume;
            }
        }

    }



    foreach ($arrayChart as $arrayChartKey => $arrayChartValue) {
      if ((stripos($arrayChartKey,"positiv") !== false)){ $Positive_vol=$arrayChartValue; }
      if ((stripos($arrayChartKey,"neutr") !== false)){ $Neutral_vol=$arrayChartValue; }
      if ((stripos($arrayChartKey,"negativ") !== false)){ $Negative_vol=$arrayChartValue; }
    }

    foreach ($arrayChartTrends as $arrayChartTrendsKey => $arrayChartTrendsValue) {
      $arrayChartTrendsValue=substr($arrayChartTrendsValue, 0, strlen($arrayChartTrendsValue)-1);
      if ((stripos($arrayChartTrendsKey,"positiv") !== false)){ $Positive_props=$arrayChartTrendsValue; }
      if ((stripos($arrayChartTrendsKey,"neutr") !== false)){ $Neutral_props=$arrayChartTrendsValue; }
      if ((stripos($arrayChartTrendsKey,"negativ") !== false)){ $Negative_props=$arrayChartTrendsValue; }
    }

    $Positive_prop=round(($Positive_vol/($Positive_vol+$Neutral_vol+$Negative_vol))*100);
    $Neutral_prop=round(($Neutral_vol/($Positive_vol+$Neutral_vol+$Negative_vol))*100);
    $Negative_prop=100-$Positive_prop-$Neutral_prop;

    $Positive_props_array=explode(",",$Positive_props);
    $Negative_props_array=explode(",",$Negative_props);
    $NPS_props="";
    for ($i=0; $i < count($Positive_props_array); $i++) { 
      $NPS_props.=($Positive_props_array[$i]-$Negative_props_array[$i]).",";
    }

    $NPS_props=substr($NPS_props, 0, strlen($NPS_props)-1);

    $chart=
    "<script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Sentiment',                                 'Percentage',         { role: 'style' },                                                              { role: 'annotation' }],
          ['  ".(($showHL) ? 'POSITIVE' : '')."',      ".$Positive_prop.",   'color: ".$config->PositiveBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Positive_prop."%'],
          ['  ".(($showHL) ? 'NEUTRAL' : '')."',       ".$Neutral_prop.",    'color: ".$config->NeutralBuzzColor." ; opacity: ".$config->BuzzOpacity."',     '".$Neutral_prop."%'],
          ['  ".(($showHL) ? 'NEGATIVE' : '')."',      ".$Negative_prop.",   'color: ".$config->NegativeBuzzColor."; opacity: ".$config->BuzzOpacity."',     '".$Negative_prop."%']
        ]);
        
        var options = {
          title: 'Related Posts=".($Positive_vol+$Neutral_vol+$Negative_vol)." | NPS=".($Positive_prop-$Negative_prop)."',
          bar: {groupWidth: \"90%\"},
          fontName: 'Open Sans',
          vAxis: {
                    maxValue:100,
                    textPosition: 'none'
                  },
          legend: { position: \"none\" },
          chartArea:{left:25,top:20,width:'90%',height:'80%'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('".$divId."'));
        chart.draw(data, options);
      }
    </script>";



    $back_values=array(
                       "chart"=>$chart,
                       "TotPos"=>$Positive_vol,
                       "TotNeu"=>$Neutral_vol,
                       "TotNeg"=>$Negative_vol,
                       "TotNPS"=>($Positive_prop-$Negative_prop),
                       "PosTrend"=>$Positive_props,
                       "NeuTrend"=>$Neutral_props,
                       "NegTrend"=>$Negative_props,
                       "NPSTrend"=>$NPS_props,
                       "Pos"=>$Positive_prop,
                       "Neu"=>$Neutral_prop,
                       "Neg"=>$Negative_prop
                      );
    return $back_values;
  }
  
  function dumpAllBuzzes(){
    global $sql;
    $monitors=$monitors=getMonitors();
    echo "Inizio <br>";
    $types=array("Twitter","Forums","Reviews","News","Facebook","Blogs");
    foreach ($monitors as $monitor) {
      echo "Monitor name: ".$monitor->name."<br>";
      ob_flush();
      flush();
      usleep(100);
      if (!$sql->select_array("monitors",array("id"=>$monitor->id))){
        echo "Monitor NON presente nel DB <br>";
        $sql->insert_array("monitors",array (   "id"=>$monitor->id,
                                                "name"=>$monitor->name,
                                                "type"=>$monitor->type,
                                                "enabled"=>$monitor->enabled,
                                                "resultsStart"=>str_replace("T", " ", $monitor->resultsStart),
                                                "resultsEnd"=>str_replace("T", " ", $monitor->resultsEnd)
                                            ));
      } else {
        echo "Monitor presente nel DB <br>";
      }
      $query="SELECT id,date FROM buzzes WHERE monitor_id = ".$monitor->id." ORDER by date DESC LIMIT 1";
      $results_from_date=$sql->fetch_all_as_array($sql->execute_query($query));
      if (!$results_from_date)
        $results_from_date=str_replace("T", " ", $monitor->resultsStart);
      else
        $results_from_date=$results_from_date[0]["date"];
      
      echo "Data di partenza: ".$results_from_date."<br>";

      while ((strtotime(substr($results_from_date, 0, 10))< strtotime(date('Y-m-d'))) ){
        $results_from_date = date('Y-m-d', strtotime($results_from_date . ' + 1 day'));
        foreach ($types as $type) {
          $buzzes=getMonitorPosts($monitor->id, $results_from_date, date('Y-m-d', strtotime($results_from_date . ' + 1 day')) ,"type:".$type);
          echo $type."> Date:".$results_from_date.": ".count($buzzes)."<br>";
          ob_flush();
          flush();
          usleep(200);
          if (count($buzzes)>0)
            foreach ($buzzes as $buzz) {
               $buzz_id= $sql->insert_array("buzzes",array (   
                                                              "monitor_id"=>$monitor->id,
                                                              "author"=>$buzz->author,
                                                              "title"=>$buzz->title,
                                                              "location"=>$buzz->location,
                                                              "contents"=>$buzz->contents,
                                                              "assignedCategoryId"=>$buzz->assignedCategoryId,
                                                              "language"=>$buzz->language,
                                                              "type"=>$buzz->type,
                                                              "date"=>str_replace("T", " ", $buzz->date),
                                                              "assignedCategoryName"=>$buzz->assignedCategoryName,
                                                              "url"=>$buzz->url,
                                                              "authorFollowers"=>$buzz->authorFollowers,
                                                              "authorFollowing"=>$buzz->authorFollowing,
                                                              "authorKlout"=>$buzz->authorKlout,
                                                              "authorPosts"=>$buzz->authorPost,
                                                              "authorGender"=>$buzz->authorGender,
                                                          ));
              foreach ($buzz->categoryScores as $categoryScore) {
                $sql->insert_array("categoryScores",array ( "buzz_id"=>$buzz_id,
                                                            "categoryName"=>$categoryScore->categoryName,
                                                            "score"=>$categoryScore->score,
                                                          ));  
              }
              
            }
        }

        
      }
    }


    $query="SELECT id,date FROM buzzes ORDER by date DESC LIMIT 1";
    $results=$sql->fetch_all_as_array($sql->execute_query($query));
    echo $results[0]["date"];
    echo "<pre>";
    print_r($results);
    echo "<pre>";

    

    

  }
?>