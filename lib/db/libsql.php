<?php



class SQLManager 
{
 	
	var $db_host = "localhost";
	var	$port="3306";
	var	$db_name = "sCRM";
	var	$db_user = "root";
	var	$db_password = "root";

  	var $conn;
  
  function SQLManager() 
  {
	
	$this->conn = mysql_connect($this->db_host, $this->db_user , $this->db_password) or die("Errore nella connessione a MySql: " . mysql_error());
	mysql_select_db($this->db_name, $this->conn) or die("Errore nella selezione del db: " . mysql_error());
	
  }
  
  function execute_query($query) { return mysql_query($query,$this->conn);  }
  
  
  function insert_array($table, $array) 
  {
    $fields_array = array_keys($array);
    $fields_str = "";
    $values_str = "";
    foreach ($fields_array as $field) {
      $fields_str .= (($fields_str === "") ? "" : ", ") . $field;
      $values_str .= (($values_str === "") ? "" : ", ") . "'" . addslashes($array[$field]) . "'";
    }
    $result = $this->execute_query("INSERT INTO $table ($fields_str) VALUES ($values_str);");
    if ($result !== FALSE) {
      return $this->last_id($result);
    }
    return -1;
  }
  
  function delete_array($table,$array)
  {
    $fields_array = array_keys($array);
    $fields_str = "";
    foreach ($fields_array as $field) {
      $fields_str .= (($fields_str === "") ? "" : " AND ") . $field . "=" . addslashes($array[$field])."";
    }
    $result = $this->execute_query("DELETE FROM $table WHERE $fields_str");
    return ($this->affected_rows($result) > 0) ? TRUE : FALSE;
  }
  
  function modify_array($table, $set_array, $condictions = NULL)
  {
    $set = array_keys($set_array);
    $set_str = "";
	foreach ($set as $field) {
	  $set_str .= (($set_str === "") ? "" : ", ") . $field . "='" . addslashes($set_array[$field]) . "'";
	}
	
	if ($condictions != NULL) {
		
		$cond=array_keys($condictions);
		
		$cond_str = " WHERE ";	
		foreach ($cond as $field) {
			$cond_str .= (($cond_str === "") ? "" : " AND ") . $field . "='" . addslashes($condictions[$field]) . "'";
		}
	
	}

	$result = $this->execute_query("UPDATE $table SET $set_str $cond_str;");
	
	if ($result !== FALSE) {
      return $this->affected_rows($result);
    }
    return -1;
  }
  
  function affected_rows($result) {
    return mysql_affected_rows();
  }

  function last_id($result) {
    return mysql_insert_id();
  }
  
  function result_num_rows($result) 
  {
    if ($result !== FALSE) { return mysql_num_rows($result); }      
    return -1;
  }
  
  function table_contains($table, $array) {
    $fields_array = array_keys($array);
    $fields_str = "";
    $i = 1;
    foreach ($fields_array as $field) {
      $fields_str .= (($fields_str === "") ? "" : " AND ") . $field . "='" . addslashes($array[$field])."'";
	  $i++;
    }
    $result = $this->execute_query("SELECT * FROM $table WHERE $fields_str");
    return ($this->result_num_rows($result) > 0) ? TRUE : FALSE;
  }


function result_to_array($result) {
    if ($result !== FALSE) {
      return mysql_fetch_array($result);
    }
    return NULL;
  }

function fetch_all_as_array($result) {
    if ($result !== FALSE) {
      $ret = array();
      while ($row = $this->result_to_array($result)) {
        $ret[] = $row;
      }
      return $ret;
    }
    return NULL;
  }
	

function select_all_as_array($table,$order = NULL,$mode = NULL) {
	$query="SELECT * FROM $table ";
	if (isset($order) && $order != NULL){
		$query.="ORDER BY $order ";
		if (isset($mode) && $mode != NULL){
			$query.=$mode;
		}
	}
    $result = $this->execute_query($query);
    return $this->fetch_all_as_array($result);
  }
  
function select_array($table,$array){
  {
	$fields_array = array_keys($array);
    $fields_str = "";
    $i = 1;
    foreach ($fields_array as $field) {
      $fields_str .= (($fields_str === "") ? "" : " AND ") . $field . "='" . addslashes($array[$field])."'";
	  $i++;
    }
    $result = $this->execute_query("SELECT * FROM $table WHERE $fields_str");
    return $this->fetch_all_as_array($result);
  }
  
}

function empty_table($table){
	return $this->execute_query("TRUNCATE TABLE $table");
}
}
?>