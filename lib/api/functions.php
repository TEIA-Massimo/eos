<?php

require_once("import.php");

$user = "TEIA_API";
$pass = "ka0liD6oidae";
$baseurl = "https://forsight.crimsonhexagon.com/api/";
$version = 3;
$assets = "assets/data";

$userTW = "44173";
$pswTW = "QbvL8CfXy9G3NCZbZuhxnXUxNa4zXR";

function doRequest($url) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


    $result = curl_exec($ch);

    curl_close($ch);
    return json_decode($result, true);

}

function auth() {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $url = $baseurl . "authenticate?version=$version&username=$user&password=$pass";

    $json = doRequest($url);

    return $json["auth"];
}

function getMonitors() {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/list?version=$version&username=$user&auth=$auth";
    $json = doRequest($url);
    $monitors = $json["monitors"];

    $objs = array();

    foreach ($monitors as $monitor) {

        $tags = array();
        foreach($monitor["tags"] as $tag) {
            $tags[] = new Tag($tag["name"]);
        }

        $objs[] = new Monitor($monitor["id"],
                              $monitor["name"],
                              $monitor["type"],
                              $monitor["enabled"],
                              $monitor["resultsStart"],
                              $monitor["resultsEnd"],
                              $tags);

    }

    return $objs;

}

function getMonitorDetailBuzz($id) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/detail?version=$version&id=$id&username=$user&auth=$auth";
    $monitor = doRequest($url);

    $tags = array();
    foreach($monitor["tags"] as $tag) {
        $tags[] = new Tag($tag["name"]);
    }

    $categories = array();
    foreach($monitor["categories"] as $category) {
        $categories[] = new Category($category["id"],
                                     $category["name"],
                                     $category["status"],
                                     $category["sortOrder"],
                                     $category["hidden"],
                                     $category["trainingDocs"]);

    }

    $obj = new MonitorBuzz($monitor["id"],
                           $monitor["name"],
                           $monitor["description"],
                           $monitor["enabled"],
                           $monitor["status"],
                           $monitor["resultsStart"],
                           $monitor["resultsEnd"],
                           $tags, $categories);

    return $obj;
}


function getMonitorDetailOpinion($id) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/detail?version=$version&id=$id&username=$user&auth=$auth";
    $monitor = doRequest($url);

    $tags = array();
    foreach($monitor["tags"] as $tag) {
        $tags[] = new Tag($tag["name"]);
    }

    $categories = array();
    foreach($monitor["categories"] as $category) {
        $categories[] = new CategoryOpinion($category["id"],
                                            $category["name"],
                                            $category["status"],
                                            $category["sortOrder"],
                                            $category["hidden"],
                                            $category["trainingDocs"], $category["groupId"], $category["group"]);

    }

    $obj = new MonitorOpinion($monitor["id"],
                              $monitor["name"],
                              $monitor["description"],
                              $monitor["enabled"],
                              $monitor["status"],
                              $monitor["resultsStart"],
                              $monitor["resultsEnd"],
                              $tags, $categories);

    return $obj;
}

function getMonitorResultsBuzz($id, $startDate, $endDate, $hideExcluded = false) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/results?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $url .= "&hideExcluded=" . ($hideExcluded ? "true" : "false");

    $json = doRequest($url);

    $objs = array();
    foreach ($json["results"] as $result) {

        $categories = array();
        foreach($result["categories"] as $category) {

            $categories[] = new CategoryResult($category["categoryId"],
                                               $category["category"],
                                               $category["proportion"],
                                               $category["volume"],
                                               $category["hidden"]
                                              );

        }
        $objs[] = new MonitorResult($result["creationDate"],
                                    $result["startDate"],
                                    $result["endDate"],
                                    $result["numberOfDocuments"],
                                    $result["numberOfRelevantDocuments"],
                                    $categories);
    }
    return $objs;
}

function getMonitorResultsOpinion($id, $startDate, $endDate, $hideExcluded = false) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/results?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $url .= "&hideExcluded=" . ($hideExcluded ? "true" : "false");

    $json = doRequest($url);

    $objs = array();
    foreach ($json["results"] as $result) {

        $categories = array();
        foreach($result["categories"] as $category) {

            $categories[] = new CategoryResultOpinion($category["categoryId"],
                    $category["category"],
                    $category["groupId"],
                    $category["group"],
                    $category["groupHidden"],
                    $category["proportion"],
                    $category["volume"],
                    $category["hidden"]
                                                     );

        }
        $objs[] = new MonitorResult($result["creationDate"],
                                    $result["startDate"],
                                    $result["endDate"],
                                    $result["numberOfDocuments"],
                                    $result["numberOfRelevantDocuments"],
                                    $categories);
    }
    return $objs;
}

function getSources($id, $startDate, $endDate) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/sources?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $json = doRequest($url);
    $objs = array();

    foreach ($json["contentSources"] as $contentSource) {

        $topSites = array();
        $keys = array_keys($contentSource["topSites"]);
        foreach($keys as $key) {
            $topSites[] = new Site($key, $contentSource["topSites"][$key]);
        }

        $sources = array();
        $keys = array_keys($contentSource["sources"]);
        foreach($keys as $key) {
            $sources[] = new Channel($key, $contentSource["sources"][$key]);
        }

        $objs[] = new TopSitesContentSources($contentSource["startDate"],
                                             $contentSource["endDate"], $topSites, $sources);

    }
    return $objs;
}

function getTwitterMetrics($id, $startDate, $endDate) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/twittermetrics?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $json = doRequest($url);
    $objs = array();

    foreach ($json["dailyResults"] as $dailyResult) {

        $topHashtags = array();
        $keys = array_keys($dailyResult["topHashtags"]);
        foreach($keys as $key) {
            $topHashtags[] = new Hashtag($key, $dailyResult["topHashtags"][$key]);
        }

        $topRetweets = array();
        foreach($dailyResult["topRetweets"] as $retweet) {
            $topRetweets[] = new Tweet($retweet["author"],
                                       $retweet["isOriginal"],
                                       $retweet["retweetCount"],
                                       $retweet["date"],
                                       $retweet["url"]);

        }

        $topMentions = array();
        $keys = array_keys($dailyResult["topMentions"]);
        foreach($keys as $key) {
            $topMentions[] = new Mention($key, $dailyResult["topMentions"][$key]);
        }

        $objs[] = new MonitorTwitterMetric($dailyResult["startDate"],
                                           $dailyResult["endDate"],
                                           $topHashtags,
                                           $topRetweets,
                                           $topMentions);

    }
    return $objs;
}

function getMonitorPosts($id, $startDate, $endDate, $filter = NULL, $extendLimit = true) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/posts?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $url .= "&extendLimit=" . ($extendLimit ? "true" : "false");

    if ($filter !== NULL) {
        $url .= "&filter=";
        if (is_array($filter)) {
            $first = true;
            foreach ($filter as $f) {
                if (!$first) {
                    $url .= "|";
                }
                $url .= $f;
                $first = false;
            }
        }
        else {
            $url .= urlencode($filter);
        }
    }

    $json = doRequest($url);
    $objs = array();

    foreach ($json["posts"] as $post) {

        $categoryScores = array();
        foreach ($post["categoryScores"] as $categoryScore) {
            $categoryScores[] = new CategoryScore($categoryScore["categoryId"],
                                                  $categoryScore["categoryName"],
                                                  $categoryScore["score"]
                                                 );
        }


        $objs[] = new Post($post["author"],
                           $post["title"],
                           $post["location"],
                           $post["contents"],
                           $post["assignedCategoryId"],
                           $post["language"],
                           $post["type"],
                           $post["date"],
                           $post["assignedCategoryName"],
                           $post["url"],
                           $post["authorFollowers"],
                           $post["authorFollowing"],
                           $post["authorKlout"],
                           $post["authorPosts"],
                           $post["authorGender"],
                           $categoryScores);

    }
    
    return $objs;
}

function getWordCloud($id, $startDate, $endDate) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/wordcloud?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $json = doRequest($url);


    $words = array();
    $keys = array_keys($json["data"]);

    foreach($keys as $key) {
        $words[] = new Word($key, $json["data"][$key]);
    }

    $wordCloud = new WordCloud($words);


    return $wordCloud;
}

function getAuthors($id, $startDate, $endDate) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;

    $auth = auth();
    $url = $baseurl . "monitor/authors?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    $json = doRequest($url);


    $objs = array();
    foreach($json["authors"] as $author) {
        $authorDetails = array();
        foreach($author["authorDetails"] as $authorDetail) {
            $authorDetails[] = new AuthorDetail(
                $authorDetail["following"],
                $authorDetail["followers"],
                $authorDetail["username"],
                $authorDetail["posts"],
                $authorDetail["kloutScore"],
                $authorDetail["detailDate"]);
        }
        $countsByAuthor = array();
        $keys = array_keys($author["countsByAuthor"]);
        foreach($keys as $key) {
            $countsByAuthor[] = new CountAuthor($key, $author["countsByAuthor"][$key]);

        }
        $objs[] = new MonitorAuthor(
            $author["startDate"],
            $author["endDate"],
            $author["numberOfAuthors"],
            $author["docsPerAuthor"],
            $author["totalImpressions"],
            $authorDetails,
            $countsByAuthor);
    }


    return $objs;
}

function generateAssetsCarrot2($id, $startDate, $endDate, $fileName, $filter = NULL) {
    global $user;
    global $pass;
    global $baseurl;
    global $version;
    global $assets;

    $auth = auth();
    $url = $baseurl . "monitor/topics?version=$version&id=$id&username=$user&auth=$auth";

    if ($startDate == NULL || $endDate == NULL) {
        die("Specificare o la data di inizio o la data di fine validit&agrave;");
    }

    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $startDate)) {
        $url .= "&start=$startDate";
    }
    else {
        die("Formato data inizio validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }
    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $endDate)) {
        $url .= "&end=$endDate";
    }
    else {
        die("Formato data fine validit&agrave; non corretto. Atteso: YYYY-MM-DD");
    }

    if ($filter !== NULL) {
        $url .= "&filter=";
        if (is_array($filter)) {
            $first = true;
            foreach ($filter as $f) {
                if (!$first) {
                    $url .= "|";
                }
                $url .= $f;
                $first = false;
            }
        }
        else {
            $url .= urlencode($filter);
        }
    }

    $path = $assets . "/" . basename($fileName);

    $json = doRequest($url);
    $xml = str_replace("\\n", "", $json["clustering"]);
    $fp = fopen($path, "w");
    fwrite($fp, $xml);
    fclose($fp);

    return $path;
}

function getTwitterMetricsTables($profileIds, $startDate, $endDate,$interval){
    global $userTW;
    global $pswTW;
    
    $profileIdsString="";
    foreach ($profileIds as $profileId){
        $profileIdsString.=$profileId.",";
    }
    
    $profileIdsString=substr($profileIdsString, 0, strlen($profileIdsString)-1);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.quintly.com/v0.9/qql?metric=twitterKeyMetricsTable&startTime=".$startDate."&endTime=".$endDate."&interval=".$interval."&profileIds=".$profileIdsString);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, "$userTW:$pswTW");

    $data = curl_exec($ch);
    curl_close($ch);
    $jsonTwT = json_decode($data, true);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.quintly.com/v0.9/qql?metric=twitterResponseTimes&startTime=".$startDate."&endTime=".$endDate."&interval=".$interval."&profileIds=".$profileIdsString);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, "$userTW:$pswTW");

    $data = curl_exec($ch);
    curl_close($ch);
    $jsonTwRT = json_decode($data, true);
    
    return array("TwitterTable"=>$jsonTwT,"ResponseTime"=>$jsonTwRT);
}

?>
