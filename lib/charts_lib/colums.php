<?php
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('Samsung-bar-chart'));
  chart.draw(data, options);
  }
</script>
?>