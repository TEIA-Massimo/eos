<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
	"Home" => APP_URL
);

/*navigation array config

ex:
"dashboard" => array(
	"title" => "Display Title",
	"url" => "http://yoururl.com",
	"url_target" => "_self",
	"icon" => "fa-home",
	"label_htm" => "<span>Add your custom label/badge html here</span>",
	"sub" => array() //contains array of sub items with the same format as the parent
)

*/
$page_nav = array(
	"dashboard" => array(
		"title" => "Dashboard",
		"url" => APP_URL,
		"icon" => "fa-home"
	),
	"Section 1 - Overall" => array(
		"title" => "Brand Overall Analysis",
		"icon" => "fa-bar-chart-o",
		"sub" => array(
			"SamsungAndCompetitors" => array(
				'title' => 'Samsung Mobile IT & Competitors',
				'url' => APP_URL.'/s11.php'
			),
			"Takeways" => array(
				'title' => 'Key Takeways',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"ShareOfVoice" => array(
				'title' => 'Share of Voice',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"TwitterMetricsMap" => array(
				'title' => 'Twitter Metrics Map',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"SentimentperSource" => array(
				'title' => 'Sentiment Analysis per Source',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"HotTopics" => array(
				'title' => 'Hot Topics',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"KeyInfluencers" => array(
				'title' => 'Key Influencers',
				'url' => APP_URL.'/smartui-carousel.php'
			)
			
		)
	),
	"Section 2 - MNO" => array(
		"title" => "MNO",
		"icon" => "fa-mobile-phone",
		"sub" => array(
			"SamsungPerception" => array(
				'title' => 'Operators perception of Samsung Mobile Italia',
				
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"ApplePerception" => array(
				'title' => 'Operators perception of Apple',
				
				'url' => APP_URL.'/smartui-carousel.php'
			)
		)
	),
	"Section 2 - CC" => array(
		"title" => "Customer Care",
		"icon" => "fa-group ",
		"sub" => array(
			"Sources&TopSites" => array(
				'title' => 'Sources and Top Sites',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"OpinionSummary" => array(
				'title' => 'Opinion Summary',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"CCHT" => array(
				'title' => 'Hot Topics',
				'url' => APP_URL.'/smartui-carousel.php'
			),
			"TwitterMetrics" => array(
				'title' => 'Twitter Metrics',
				'url' => APP_URL.'/smartui-carousel.php'
			)
			
		)
	),
	"Reports" => array(
		"title" => "Reports",
		"icon" => "fa-list-alt",
	)
);

//configuration variables
$page_title = "";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>