<?php 
	header( 'Content-type: text/plan; charset=utf-8' );
	header('Cache-Control: no-cache');

	
	include("../../lib/api/functions.php");
  	include("../../lib/db/libsql.php");
	
	ini_set("memory_limit","512M");
	ini_set('output_buffering', 'off');
	ini_set('implicit_flush', true);
	
	ob_implicit_flush(true);
	set_time_limit(0);

	$sql = new SQLManager();
	$types=array("Twitter","Forums","Reviews","News","Facebook","Blogs");
   	$total_monitors=0;

	echo "Inizio: ".date("F j, Y, g:i a")."\n";
	echo"---------------------------------------\n\n";
	ob_flush();
    flush();
    usleep(100);
	
	$monitors=getMonitors();
	foreach ($monitors as $monitor) {
		$total_monitors++;
		echo "Monitor: ".$monitor->name."\n";
		echo "id: 312098789\n";
		if (!$sql->select_array("monitors",array("id"=>$monitor->id))){
        	echo "Status: NOT Present\n\n";
        } else {
        	echo "Status: Present\n\n";
        	$sql->insert_array("monitors",array (   "id"=>$monitor->id,
                                                	"name"=>$monitor->name,
                                                	"type"=>$monitor->type,
                                                	"enabled"=>$monitor->enabled,
                                                	"resultsStart"=>str_replace("T", " ", $monitor->resultsStart),
                                                	"resultsEnd"=>str_replace("T", " ", $monitor->resultsEnd)
                                            	));
        }

		echo " ".str_pad("", 14," ",STR_PAD_BOTH);
		foreach ($types as $type) {
			echo "|".str_pad($type, 14," ",STR_PAD_BOTH);
		}
		echo "\n\n\n\n";
	}

	echo "Total monitors amount: ".$total_monitors."\n";
	//str_pad("414", 4," ",STR_PAD_LEFT)."|\n";


	//dumpAllBuzzes();

?>

