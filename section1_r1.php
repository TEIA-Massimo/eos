<?php

//initilize the page
require_once("inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

//library of charts functions
require_once("lib/google-chart-layer/chart_Layer.php");
/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav["Section 1 - Overall"]["sub"]["SamsungAndCompetitors"]["active"] = true;
include("inc/nav.php");

//scripts for charts

?>
<!-- ==========================CHARTS========================== -->
<!-- Samsung Chart / Report 1-->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('Samsung-bar-chart'));
  chart.draw(data, options);
  }
</script>

<!-- Nokia Chart / Report 1-->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('Nokia-bar-chart'));
  chart.draw(data, options);
  }
</script>

<!-- LG Chart / Report 1-->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('LG-bar-chart'));
  chart.draw(data, options);
  }
</script>
<!-- Apple Chart / Report 1-->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('Apple-bar-chart'));
  chart.draw(data, options);
  }
</script>


<!-- Huawei Chart / Report 1-->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Sentiment', 'Percentage', { role: 'style' }, { role: 'annotation' }],
      ['  POSITIVE',      0,   'color: green; opacity: 0.4',     '0%'],
      ['  NEUTRAL',       0,    'color: #e5e4e2 ; opacity: 0.4',     '0%'],
      ['  NEGATIVE',      100,   'color: red; opacity: 0.4',     '100%']]);
        
    var options = {
      title: 'Related Posts=0 | NPS=-100',
      bar: {groupWidth: "90%"},
      fontName: 'Open Sans',
      vAxis: {
        maxValue:100,
        textPosition: 'none'
      },
      legend: { position: "none" },
      chartArea:{left:25,top:20,width:'90%',height:'80%'}
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('Huawei-bar-chart'));
  chart.draw(data, options);
  }
</script>

<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		$breadcrumbs["Samsung Mobile Italy & Competitors"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<h1 class="page-title txt-color-blueDark"><i class="fa fa-bar-chart-o fa-fw "></i> Section Overall <span>> Samsung Mobile Italia & Competitor</span></h1>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
				<ul id="sparks" class="">
					<li class="sparks-info">
						<h5> Positive Trend <span class="txt-color-green"><?php echo (($SamsungBuzz["Pos"]<$SamsungBuzz_old["Pos"]) ? $arrow_down : $arrow_up)." ". $SamsungBuzz["TotPos"]; ?></span></h5>
						<div class="sparkline txt-color-green hidden-mobile hidden-md hidden-sm">
							<?php echo $SamsungBuzz["PosTrend"]; ?>
						</div>
					</li>
					<li class="sparks-info">
						<h5> Neutral Trend <span class="txt-color-grey"><?php echo (($SamsungBuzz["Neu"]<$SamsungBuzz_old["Neu"]) ? $arrow_down : $arrow_up)." ". $SamsungBuzz["TotNeu"]; ?></span></h5>
						<div class="sparkline txt-color-grey hidden-mobile hidden-md hidden-sm">
							<?php echo $SamsungBuzz["NeuTrend"]; ?>
						</div>
					</li>
					<li class="sparks-info">
						<h5> Negative Trend <span class="txt-color-red"><?php echo (($SamsungBuzz["Neg"]<$SamsungBuzz_old["Neg"]) ? $arrow_down : $arrow_up)." ". $SamsungBuzz["TotNeg"]; ?></span></h5>
						<div class="sparkline txt-color-red hidden-mobile hidden-md hidden-sm">
							<?php echo $SamsungBuzz["NegTrend"]; ?>
						</div>
					</li>
					<li class="sparks-info">
						<h5> NPS Trend <span class="txt-color-blue"><?php echo (($SamsungBuzz["TotNPS"]<$SamsungBuzz_old["TotNPS"]) ? $arrow_down : $arrow_up)." ". $SamsungBuzz["TotNPS"]; ?></span></h5>
						<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
							<?php echo $SamsungBuzz["NPSTrend"]; ?>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-first" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>Samnsung Mobile</h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<div id="Samsung-bar-chart" class="chart"></div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats">

												
													<div class="smart-form" action="#">
														<header>
															Select time range
														</header>
														<fieldset>
															<div class="row">
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> 
																	<label class="input"> <i class="icon-append fa fa-calendar"></i>
																		<input type="text" name="startdate" id="startdate" placeholder="Start date">
																	</label>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> 
																	<label class="input"> <i class="icon-append fa fa-calendar"></i>
																		<input type="text" name="enddate" id="enddate" placeholder="End date">
																	</label>
																</div>
															</div>
														</fieldset>
														<footer>
															<button type="submit" class="btn btn-primary" onclick="javascript:alert('ciao');">
																Update
															</button>
														</footer>
													</div>
													
													
											

											</div>
						</div>
					</div>
				</article>
				<article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="jarviswidget" id="wid-id-second" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>Samnsung TOP Sites </h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<div id="Samsung-Top-Sites" class="chart"></div>
							</div>
						</div>
					</div>
				</article>
			</div>
			<div class="row">
				<article class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
					<div class="jarviswidget" id="wid-id-3rd" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>Nokia</h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<div id="Nokia-bar-chart" class="chart"></div>
							</div>
						</div>
					</div>
				</article>
				<article class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
					<div class="jarviswidget" id="wid-id-4th" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>Apple</h2>
						</header>
						<div>
							<div class="jarviswidget-editbox">
							</div>
							<div class="widget-body no-padding">
								<div id="Apple-bar-chart" class="chart"></div>
							</div>
						</div>
					</div>
				</article>
				<article class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
					<div class="jarviswidget" id="wid-id-5th" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>LG</h2>
						</header>
						<div>
							<div class="jarviswidget-editbox">
							</div>
							<div class="widget-body no-padding">
								<div id="LG-bar-chart" class="chart"></div>
							</div>
						</div>
					</div>
				</article>
				<article class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
					<div class="jarviswidget" id="wid-id-6th" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
							<h2>Huawei</h2>
						</header>
						<div>
							<div class="jarviswidget-editbox">
							</div>
							<div class="widget-body no-padding">
								<div id="Huawei-bar-chart" class="chart"></div>
							</div>
						</div>
					</div>
				</article>
				<!-- WIDGET END -->
			</div>
			<!-- end row -->
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	// include page footer
	include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
<script type="text/javascript">
$(document).ready(function() {
	$('#startdate').datepicker({
			dateFormat : 'dd.mm.yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#finishdate').datepicker('option', 'minDate', selectedDate);
			}
		});
	$('#enddate').datepicker({
			dateFormat : 'dd.mm.yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
			onSelect : function(selectedDate) {
				$('#finishdate').datepicker('option', 'minDate', selectedDate);
			}
		});

 })
</script>



