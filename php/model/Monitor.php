<?php

class Monitor {
    public $id;
    public $name;
    public $type;
    public $enabled;
    public $resultsStart;
    public $resultsEnd;
    public $tags;

    public function __construct($id,
                                $name,
                                $type,
                                $enabled,
                                $resultsStart,
                                $resultsEnd,
                                $tags) {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->enabled = $enabled;
        $this->resultsStart = $resultsStart;
        $this->resultsEnd = $resultsEnd;
        $this->tags = $tags;
    }
}
?>
