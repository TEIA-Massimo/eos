<?php

require_once("Monitor.php");

class MonitorOpinion extends Monitor {

    public $description;
    public $status;
    public $categories;

    public function __construct($id,
                                $name,
                                $description,
                                $enabled,
                                $status,
                                $resultsStart,
                                $resultsEnd,
                                $tags, $categories) {

        $this->type = "OPINION";
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->enabled = $enabled;
        $this->status = $status;
        $this->resultsStart = $resultsStart;
        $this->resultsEnd = $resultsEnd;
        $this->tags = $tags;
        $this->categories = $categories;
    }
}

?>
