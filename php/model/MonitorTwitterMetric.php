<?php
class MonitorTwitterMetric {

    public $startDate;
    public $endDate;
    public $topHashtags;
    public $topRetweets;
    public $topMentions;

    public function __construct($startDate,
                                $endDate,
                                $topHashtags,
                                $topRetweets,
                                $topMentions) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->topHashtags = $topHashtags;
        $this->topRetweets = $topRetweets;
        $this->topMentions = $topMentions;
    }
}

?>
