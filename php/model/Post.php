<?php
class Post {

    public $author;
    public $title;
    public $location;
    public $contents;
    public $assignedCategoryId;
    public $language;
    public $type;
    public $date;
    public $assignedCategoryName;
    public $url;
    public $authorFollowers;
    public $authorFollowing;
    public $authorKlout;
    public $authorPosts;
    public $authorGender;
    public $categoryScores;

    public function __construct($author,
                                $title,
                                $location,
                                $contents,
                                $assignedCategoryId,
                                $language,
                                $type,
                                $date,
                                $assignedCategoryName,
                                $url,
                                $authorFollowers,
                                $authorFollowing,
                                $authorKlout,
                                $authorPosts,
                                $authorGender,
                                $categoryScores) {
        $this->author = $author;
        $this->title = $title;
        $this->location = $location;
        $this->contents = $contents;
        $this->assignedCategoryId = $assignedCategoryId;
        $this->language = $language;
        $this->type = $type;
        $this->date = $date;
        $this->assignedCategoryName = $assignedCategoryName;
        $this->url = $url;
        $this->authorFollowers = $authorFollowers;
        $this->authorFollowing = $authorFollowing;
        $this->authorKlout = $authorKlout;
        $this->authorPosts = $authorPosts;
        $this->authorGender = $authorGender;
        $this->categoryScores = $categoryScores;
    }

}
?>
