<?php
class AuthorDetail {
    public $following;
    public $followers;
    public $username;
    public $posts;
    public $kloutScore;
    public $detailDate;

    public function __construct($following,
                                $followers,
                                $username,
                                $posts,
                                $kloutScore,
                                $detailDate) {

        $this->following = $following;
        $this->followers = $followers;
        $this->username = $username;
        $this->posts = $posts;
        $this->kloutScore = $kloutScore;
        $this->detailDate = $detailDate;

    }
}
?>
