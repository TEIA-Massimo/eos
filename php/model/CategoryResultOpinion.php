<?php
require_once("CategoryResult.php");

class CategoryResultOpinion extends CategoryResult {

    public $groupId;
    public $group;
    public $groupHidden;

    public function __construct($categoryId,
                                $category,
                                $groupId,
                                $group,
                                $groupHidden,
                                $proportion,
                                $volume,
                                $hidden) {
        $this->categoryId = $categoryId;
        $this->category = $category;
        $this->groupId = $groupId;
        $this->group = $group;
        $this->groupHidden = $groupHidden;
        $this->proportion = $proportion;
        $this->volume = $volume;
        $this->hidden = $hidden;
    }
}
