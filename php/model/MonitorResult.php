<?php

class MonitorResult {

    public $creationDate;
    public $startDate;
    public $endDate;
    public $numberOfDocuments;
    public $numberOfRelevantDocuments;
    public $categories;

    public function __construct($creationDate,
                                $startDate,
                                $endDate,
                                $numberOfDocuments,
                                $numberOfRelevantDocuments,
                                $categories) {
        $this->creationDate = $creationDate;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->numberOfDocuments = $numberOfDocuments;
        $this->numberOfRelevantDocuments = $numberOfRelevantDocuments;
        $this->categories = $categories;
    }
}
?>
