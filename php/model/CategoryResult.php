<?php
class CategoryResult {

    public $categoryId;
    public $category;
    public $proportion;
    public $volume;
    public $hidden;



    public function __construct($categoryId,
                                $category,
                                $proportion,
                                $volume,
                                $hidden) {
        $this->categoryId = $categoryId;
        $this->category = $category;
        $this->proportion = $proportion;
        $this->volume = $volume;
        $this->hidden = $hidden;
    }
}
