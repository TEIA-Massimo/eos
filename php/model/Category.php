<?php
class Category {

    public $id;
    public $name;
    public $status;
    public $sortOrder;
    public $hidden;
    public $trainingDocs;

    public function __construct($id,
                                $name,
                                $status,
                                $sortOrder,
                                $hidden,
                                $trainingDocs) {

        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
        $this->sortOrder = $sortOrder;
        $this->hidden = $hidden;
        $this->trainingDocs = $trainingDocs;


    }
}
