<?php

require_once("Category.php");

class CategoryOpinion extends Category {

    public $groupId;
    public $group;

    public function __construct($id,
                                $name,
                                $status,
                                $sortOrder,
                                $hidden,
                                $trainingDocs, $groupId, $group) {

        $this->id = $id;
        $this->name = $name;
        $this->status = $status;
        $this->sortOrder = $sortOrder;
        $this->hidden = $hidden;
        $this->trainingDocs = $trainingDocs;
        $this->groupId = $groupId;
        $this->group = $group;
    }
}
