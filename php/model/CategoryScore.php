<?php
class CategoryScore {
    public $categoryId;
    public $categoryName;
    public $score;

    public function __construct($categoryId,
                                $categoryName,
                                $score) {
        $this->categoryId = $categoryId;
        $this->categoryName = $categoryName;
        $this->score = $score;
    }
}
?>
