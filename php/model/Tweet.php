<?php
class Tweet {
    public $author;
    public $isOriginal;
    public $retweetCount;
    public $date;
    public $url;

    public function __construct($author,
                                $isOriginal,
                                $retweetCount,
                                $date,
                                $url
                               ) {

        $this->author = $author;
        $this->isOriginal = $isOriginal;
        $this->retweetCount = $retweetCount;
        $this->date = $date;
        $this->url = $url;

    }
}
?>
