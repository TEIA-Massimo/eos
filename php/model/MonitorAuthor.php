<?php
class MonitorAuthor {
    public $startDate;
    public $endDate;
    public $numberOfAuthors;
    public $docsPerAuthor;
    public $totalImpressions;
    public $authorDetails;
    public $countsByAuthor;

    public function __construct($startDate,
                                $endDate,
                                $numberOfAuthors,
                                $docsPerAuthor,
                                $totalImpressions,
                                $authorDetails,
                                $countsByAuthor) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->numberOfAuthors = $numberOfAuthors;
        $this->docsPerAuthor = $docsPerAuthor;
        $this->totalImpressions = $totalImpressions;
        $this->authorDetails = $authorDetails;
        $this->countsByAuthor = $countsByAuthor;
    }
}


?>
