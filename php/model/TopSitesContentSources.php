<?php
class TopSitesContentSources {
    public $startDate;
    public $endDate;
    public $topSites;
    public $sources;

    public function __construct($startDate, $endDate, $topSites, $sources) {

        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->topSites = $topSites;
        $this->sources = $sources;
    }
}
?>
