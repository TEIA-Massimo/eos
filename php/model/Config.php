<?php
  class Config {
    public $PositiveBuzzColor;
    public $NeutralBuzzColor;
    public $NegativeBuzzColor;
    public $NPSBuzzColor;
    public $BuzzOpacity;
    public $BrandsColors;

    public function __construct() {

        $this->PositiveBuzzColor="green";
        $this->NeutralBuzzColor="#e5e4e2";
        $this->NegativeBuzzColor="red";
        $this->NPSBuzzColor="#333333";
        $this->BuzzOpacity="0.4";
        $this->BrandsColors=array("Samsung"=>"blue","LG"=>"red","Nokia"=>"green","Hwawei"=>"orange","Apple"=>"silver");
    }
}
  
  
?>